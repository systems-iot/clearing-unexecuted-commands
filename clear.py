from iotery_python_server_sdk import Iotery

# Get your Account Manager API KEY here: https://iotery.io/system (API KEY)
API_KEY = "your-api-key"

DEVICE_UUID = "your-device-uuid"

iotery = Iotery(API_KEY)

device = iotery.clearUnexecutedDeviceCommandInstances(deviceUuid=DEVICE_UUID,
    data={})

print(device)