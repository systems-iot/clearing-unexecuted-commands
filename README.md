# Clearing Unexecuted Commands for an Iotery Device

Getting:

```
git clone https://gitlab.com/systems-iot/clearing-unexecuted-commands.git
```

Then cd into the directory:

```
cd clearing-unexecuted-commands
```

Create a virtual env:

```
virtualenv venv
```

Activate it (on MacOS/Linux):

```
source venv/bin/activate
```

Windows:

```
venv\Scripts\activate.bat
```

Install:

```
pip install -r requirements.txt
```

Then get your API Key and your `device uuid` from Iotery, and replace them appropriately in `clear.py`:

```python
from iotery_python_server_sdk import Iotery

# Get your Account Manager API KEY here: https://iotery.io/system (API KEY)
API_KEY = "your-api-key"

DEVICE_UUID = "your-device-uuid"

iotery = Iotery(API_KEY)

device = iotery.clearUnexecutedDeviceCommandInstances(deviceUuid=DEVICE_UUID,
    data={})

print(device)
```

### Running

In your virtualenv:

```
python clear.py
```

It should print `{'status': 'success'}`
